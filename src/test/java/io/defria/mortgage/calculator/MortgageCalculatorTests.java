package io.defria.mortgage.calculator;


import io.defria.mortgage.common.BaseTest;
import io.defria.mortgage.repository.dataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

public class MortgageCalculatorTests extends BaseTest {
    @Test(dataProvider = "testData", dataProviderClass = dataProvider.class)
    public void calculateMortgage_whenCalled_calculatesMortgage(Map<String, String> testData) {
        // Arrange
        double expectedResult = Double.parseDouble(testData.get("expectedMonthlyRepayment"));
        MortgageCalculator calculator = getCalculator(testData);

        // Act
        double result = calculator.calculateMortgage();

        // Assert
        Assert.assertEquals(result, expectedResult);
    }

    @Test(dataProvider = "testData", dataProviderClass = dataProvider.class)
    public void getRemainingBalances_whenCalled_getsRemainingBalances(Map<String, String> testData) {
        // Arrange
        int expectedResult = Integer.parseInt(testData.get("expectedNumberOfRepayments"));
        MortgageCalculator calculator = getCalculator(testData);

        // Act
        double[] result = calculator.getRemainingBalances();

        // Assert
        Assert.assertEquals(result.length, expectedResult);
    }
}
