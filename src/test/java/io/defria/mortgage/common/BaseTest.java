package io.defria.mortgage.common;

import io.defria.mortgage.calculator.MortgageCalculator;
import io.defria.mortgage.model.Mortgage;

import java.util.Map;

public abstract class BaseTest {

    protected MortgageCalculator getCalculator(Map<String, String> testData) {
        Mortgage mortgage = getMortgage(testData);
        return new MortgageCalculator(mortgage.getPrincipal(), mortgage.getAnnualInterestRate(), mortgage.getYears());
    }

    private Mortgage getMortgage(Map<String, String> testData) {
        Mortgage mortgage = new Mortgage();
        mortgage.setPrincipal(Double.parseDouble(testData.get("principal")));
        mortgage.setYears(Byte.parseByte(testData.get("period")));
        mortgage.setAnnualInterestRate(Float.parseFloat(testData.get("annualInterest")));
        return mortgage;
    }
}
