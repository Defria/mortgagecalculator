package io.defria.mortgage.reporting;

import io.defria.mortgage.calculator.MortgageCalculator;
import io.defria.mortgage.common.BaseTest;
import io.defria.mortgage.repository.dataProvider;
import org.testng.annotations.Test;

import java.util.Map;

public class MortgageReportTests extends BaseTest {

    @Test(dataProvider = "testData", dataProviderClass = dataProvider.class)
    public void printMonthlyPayment_whenCalled_printsMonthlyRepayment(Map<String, String> testData) {
        // Arrange
        MortgageCalculator calculator = getCalculator(testData);
        MortgageReport report = getReport(calculator);

        // Act
        report.printMonthlyPayment();
    }

    @Test(dataProvider = "testData", dataProviderClass = dataProvider.class)
    public void printPaymentSchedule_whenCalled_printsPaymentSchedule(Map<String, String> testData) {
        // Arrange
        MortgageCalculator calculator = getCalculator(testData);
        MortgageReport report = getReport(calculator);

        // Act
        report.printPaymentSchedule();
    }

    private MortgageReport getReport(MortgageCalculator calculator) {
        return new MortgageReport(calculator);
    }
}
