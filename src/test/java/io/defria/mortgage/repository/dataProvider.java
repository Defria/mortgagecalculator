package io.defria.mortgage.repository;

import com.opencsv.CSVReader;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileReader;
import java.util.*;

public class dataProvider {

    @DataProvider(name = "testData")
    public static Iterator<Object[]> csvReader() {
        String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "testData" + File.separator + "mortgagesTestData.csv";
        List<Object[]> data = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new FileReader(filePath))) {
            String[] header = reader.readNext();
            String[] line;
            while ((line = reader.readNext()) != null) {
                Map<String, String> map = new HashMap<>();
                for (int i = 0; i < header.length; i++) {
                    map.put(header[i], line[i]);
                }
                data.add(new Object[]{map});
            }
        } catch (Exception e) {
            throw new RuntimeException("Error processing file: " + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        }
        return data.iterator();
    }
}
