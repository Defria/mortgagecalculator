package io.defria.mortgage;

import io.defria.mortgage.startup.MortgageApp;

public class Main {

    public static void main(String[] args) {
        MortgageApp.start();
    }
}
