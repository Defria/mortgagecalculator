package io.defria.mortgage.reporting;

import io.defria.mortgage.calculator.MortgageCalculator;

import java.text.NumberFormat;

public class MortgageReport {
    private final MortgageCalculator calculator;

    public MortgageReport(MortgageCalculator calculator) {
        this.calculator = calculator;
    }

    public void printPaymentSchedule() {
        System.out.println();
        System.out.println("PAYMENT SCHEDULE");
        System.out.println("----------------");
        for (double balance : calculator.getRemainingBalances())
            System.out.println(formatCurrency(balance));
    }

    private String formatCurrency(double balance) {
        return NumberFormat.getCurrencyInstance().format(balance);
    }

    public void printMonthlyPayment() {
        System.out.println();
        System.out.println();
        System.out.println("MORTGAGE");
        System.out.println("--------");
        System.out.println(String.format("Monthly Payment: %s", formatCurrency(calculator.calculateMortgage())));
    }
}
