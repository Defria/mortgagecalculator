package io.defria.mortgage.calculator;

public class MortgageCalculator {
    private final static byte MONTHS_IN_YEAR = 12;
    private final static byte PERCENT = 100;
    private double principal;
    private float annualInterestRate;
    private byte period;

    public MortgageCalculator(double principal, float annualInterestRate, byte years) {
        setPrincipal(principal);
        setAnnualInterestRate(annualInterestRate);
        setPeriod(years);
    }

    private double calculateBalance(short numberOfPaymentMade) {
        return principal * (Math.pow((1 + getMonthlyInterest()), getNumberOfPayment()) - (Math.pow(1 + getMonthlyInterest(), numberOfPaymentMade))) / ((Math.pow((1 + getMonthlyInterest()), getNumberOfPayment())) - 1);
    }

    private float getMonthlyInterest() {
        return getAnnualInterestRate() / PERCENT / MONTHS_IN_YEAR;
    }

    private short getNumberOfPayment() {
        return (short) (getPeriod() * MONTHS_IN_YEAR);
    }

    private double getPrincipal() {
        return principal;
    }

    private void setPrincipal(double principal) {
        this.principal = principal;
    }

    private float getAnnualInterestRate() {
        return annualInterestRate;
    }

    private void setAnnualInterestRate(float annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    private byte getPeriod() {
        return period;
    }

    private void setPeriod(byte period) {
        this.period = period;
    }

    public double calculateMortgage() {
        double result = (getMonthlyInterest() * getPrincipal()) / (1 - Math.pow((1 + getMonthlyInterest()), getNumberOfPayment() * -1));
        return Math.floor(result * PERCENT) / PERCENT;
    }

    public double[] getRemainingBalances() {
        short numberOfPayment = getNumberOfPayment();
        double[] balances = new double[numberOfPayment];
        for (short month = 1; month <= numberOfPayment; month++) {
            balances[month - 1] = calculateBalance(month);
        }
        return balances;
    }
}
