package io.defria.mortgage.startup;

import io.defria.mortgage.calculator.MortgageCalculator;
import io.defria.mortgage.reporting.MortgageReport;

public class MortgageApp {
    public static void start() {
        double principal = ConsoleReader.readNumber("Principal/Loan", 100_000, 50_000_000);
        float annualInterestRate = (float) ConsoleReader.readNumber("Annual Interest Rate", 1.00, 25.00);
        byte years = (byte) ConsoleReader.readNumber("Period (Years)", 1, 30);

        MortgageCalculator calculator = new MortgageCalculator(principal, annualInterestRate, years);
        MortgageReport report = new MortgageReport(calculator);
        report.printMonthlyPayment();
        report.printPaymentSchedule();
    }
}
