package io.defria.mortgage.startup;

import java.util.Scanner;

public class ConsoleReader {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static double readNumber(String prompt, double min, double max) {
        System.out.print(String.format("%s: ", prompt));
        double value = Double.parseDouble(SCANNER.nextLine().trim());
        while (value < min || value > max) {
            System.out.println(String.format("Please enter value between %s and %s", min, max));
            System.out.print(String.format("%s: ", prompt));
            value = Double.parseDouble(SCANNER.nextLine().trim());
        }
        return value;
    }
}
